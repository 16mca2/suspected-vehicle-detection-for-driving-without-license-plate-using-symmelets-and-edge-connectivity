﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="view_user_manage.aspx.cs" Inherits="view_user_manage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                    onitemcommand="DataGrid1_ItemCommand" BackColor="White" 
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="0" 
                    CellSpacing="5" ForeColor="Black" Height="185px" Width="596px" 
                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="Small" 
                    Font-Strikeout="False" Font-Underline="False">
                    <AlternatingItemStyle BackColor="White" />
                    <Columns>
                        <asp:BoundColumn DataField="u_id" HeaderText="ID"></asp:BoundColumn>
                        <asp:BoundColumn DataField="name" HeaderText="NAME"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dob" HeaderText="DOB"></asp:BoundColumn>
                        <asp:BoundColumn DataField="gender" HeaderText="GENDER"></asp:BoundColumn>
                        <asp:BoundColumn DataField="address" HeaderText="ADDRESS"></asp:BoundColumn>
                        <asp:BoundColumn DataField="email" HeaderText="E-MAIL"></asp:BoundColumn>
                        <asp:ButtonColumn CommandName="Accept" Text="Accept"></asp:ButtonColumn>
                        <asp:ButtonColumn CommandName="Reject" Text="Reject"></asp:ButtonColumn>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <ItemStyle BackColor="#F7F7DE" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                        Mode="NumericPages" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>

