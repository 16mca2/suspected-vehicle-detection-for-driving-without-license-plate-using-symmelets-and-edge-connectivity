﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class view_user_manage : System.Web.UI.Page
{
    dboperations db = new dboperations();
    SqlCommand cmd = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cmd.CommandText = "select * from user_registration where status='pending'";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Accept")
        {
            cmd.CommandText = "update user_registration set  status='Accept' where u_id='" + e.Item.Cells[0].Text + "' ";
            db.execute(cmd);

            cmd.CommandText = "select * from user_registration where status='pending'";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
        else
        {
            cmd.CommandText = "update user_registration set status='Reject' where u_id='" + e.Item.Cells[0].Text + "' ";
            db.execute(cmd);

            cmd.CommandText = "select * from user_registration where ststus='pending'";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
    }
}