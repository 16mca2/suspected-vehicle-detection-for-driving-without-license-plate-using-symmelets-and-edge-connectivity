﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="view_user_details.aspx.cs" Inherits="add_offence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 336px;
        }
        .style3
        {
            text-align: center;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td class="style3">
                        Select User</td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
                        <table class="style1">
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                        Height="141px" style="margin-left: 0px" Width="307px">
                                        <Columns>
                                            <asp:BoundColumn DataField="vh_id" HeaderText="ID"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="u_id" HeaderText="USER"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="vehicle" HeaderText="VEHICLE"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                        </asp:View>
    </asp:MultiView>
</asp:Content>

