﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="a_registration.aspx.cs" Inherits="a_registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style4
        {
            width: 172px;
            height: 46px;
        }
        .style5
        {
            height: 46px;
        }
        .style6
        {
            width: 402px;
            height: 46px;
        }
        .style7
        {
            width: 172px;
            height: 53px;
        }
        .style8
        {
            height: 53px;
        }
        .style9
        {
            width: 402px;
            height: 53px;
        }
        .style10
        {
            width: 172px;
            height: 44px;
        }
        .style11
        {
            height: 44px;
        }
        .style12
        {
            width: 402px;
            height: 44px;
        }
        .style13
        {
            width: 172px;
            height: 41px;
        }
        .style14
        {
            height: 41px;
        }
        .style15
        {
            width: 402px;
            height: 41px;
        }
        .style17
        {
            height: 46px;
            width: 197px;
        text-align: center;
    }
        .style18
        {
            height: 53px;
            width: 197px;
        text-align: center;
    }
        .style19
        {
            height: 44px;
            width: 197px;
        text-align: center;
    }
        .style20
        {
            height: 41px;
            width: 197px;
        text-align: center;
    }
        .style22
        {
            width: 172px;
            height: 47px;
        }
        .style23
        {
            height: 47px;
            width: 197px;
        text-align: center;
    }
        .style24
        {
            width: 402px;
            height: 47px;
        }
        .style25
        {
            height: 47px;
        }
        .style26
        {
            width: 172px;
            height: 45px;
        }
        .style27
        {
            height: 45px;
            width: 197px;
        }
        .style28
        {
            width: 402px;
            height: 45px;
        }
        .style29
        {
            height: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand" Height="199px" Width="542px" 
                Font-Bold="False" Font-Italic="False" 
                Font-Overline="False" Font-Size="Small" Font-Strikeout="False" 
                Font-Underline="False">
                <Columns>
                    <asp:BoundColumn DataField="a_id" HeaderText="ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="name" HeaderText="NAME"></asp:BoundColumn>
                    <asp:BoundColumn DataField="address" HeaderText="ADDRESS"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ph" HeaderText="CONTACT"></asp:BoundColumn>
                    <asp:BoundColumn DataField="email" HeaderText="E-MAIL"></asp:BoundColumn>
                    <asp:ButtonColumn Text="manage"></asp:ButtonColumn>
                </Columns>
            </asp:DataGrid>
            &nbsp;<asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                Text="ADD NEW" Width="126px" />
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td class="style17">
                        NAME</td>
                    <td class="style6">
                        <asp:TextBox ID="TextBox1" runat="server" ontextchanged="TextBox1_TextChanged"></asp:TextBox>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style5">
                    </td>
                </tr>
                <tr>
                    <td class="style7">
                    </td>
                    <td class="style18">
                        ADDRESS</td>
                    <td class="style9">
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="TextBox2" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style8">
                    </td>
                </tr>
                <tr>
                    <td class="style10">
                    </td>
                    <td class="style19">
                        CONTACT</td>
                    <td class="style12">
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                        &nbsp;
                        </td>
                    <td class="style11">
                    </td>
                </tr>
                <tr>
                    <td class="style13">
                    </td>
                    <td class="style20">
                        <asp:Label ID="Label2" runat="server" Text="E-MAIL"></asp:Label>
                    </td>
                    <td class="style15">
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                        &nbsp;
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="TextBox4" ErrorMessage="e-mail!" ForeColor="Red" 
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                            ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style14">
                    </td>
                </tr>
                <tr>
                    <td class="style22">
                    </td>
                    <td class="style23">
                        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td class="style24">
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                            ControlToValidate="TextBox5" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style25">
                    </td>
                </tr>
                <tr>
                    <td class="style26">
                    </td>
                    <td class="style27">
                    </td>
                    <td class="style28">
                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click1" 
                            Text="REGISTER" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="UPDATE" />
                    </td>
                    <td class="style29">
                    </td>
                </tr>
            </table>
            </asp:View>
    </asp:MultiView>
</asp:Content>

